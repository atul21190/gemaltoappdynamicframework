//
//  Constants.h
//  DynamicFrameWork
//
//  Created by Atul Kumar on 6/18/19.
//  Copyright © 2019 Atul Kumar. All rights reserved.
//

#ifndef Constants_h
#define Constants_h



#define baseUrl @"https://api.themoviedb.org/3/"
#define searchMoviefor2017Release  @"search/movie&primary_release_year=2017"
#define searchMoviefor2018Release  @"search/movie&primary_release_year=2018"
#define discoverMoviefor2017Release  @"discover/movie?api_key=%@&primary_release_year=2017&sort_by=popularity.desc"
#define discoverMoviefor2018Release  @"discover/movie&primary_release_year=2018&sort_by=popularity.desc"
#define ApiKey  @"c41a5f07878255babc67e2b74d06e1aa"
#define totalMovieCountToShow 10
#define EncryptionKey  @"EncryptionKey"
#define CipherText  @"CipherText"
#define ApiKeyText  @"ApiKeyText"


#endif /* Constants_h */
