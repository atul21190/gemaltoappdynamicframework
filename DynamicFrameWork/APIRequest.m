//
//  APIRequest.m
//  DynamicFrameWork
//
//  Created by Atul Kumar on 6/14/19.
//  Copyright © 2019 Atul Kumar. All rights reserved.
//

#import "APIRequest.h"
#import <SortFunctionStaticLibrary/SortFunction.h>
#import "Constants.h"


@implementation APIRequest
NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

// Singlton class
+(APIRequest*)getInstance {
    static APIRequest *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[APIRequest alloc] init];
        [sharedInstance encryptKey];
    });
    return sharedInstance;
}
// method to generate random string of given length
-(NSString *) randomStringWithLength: (int) len {
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
    }
    
    return randomString;
}
// encrypting apikey and saving cipher text to userdefaults.
-(void)encryptKey {
    NSString *key = [self randomStringWithLength:(int)[ApiKey length]];
    [[NSUserDefaults standardUserDefaults] setObject:key forKey:EncryptionKey];
    NSString *cipher = [self encryptAndDecrypt:key key2:ApiKey];
    [[NSUserDefaults standardUserDefaults] setObject:cipher forKey:CipherText];
}
-(NSMutableArray*)splitStringIntoChars:(NSString*)argStr{
    NSMutableArray *characters = [[NSMutableArray alloc]
                                  initWithCapacity:[argStr length]];
    for (int i=0; i < [argStr length]; i++)
    {
        NSString *ichar = [NSString stringWithFormat:@"%c", [argStr characterAtIndex:i ]];
        [characters addObject:ichar];
    }
    return characters;
}
-(NSString*)encryptAndDecrypt:(NSString*)key1 key2:(NSString*)key2 {
    
    NSMutableArray *hexArray1 = [self splitStringIntoChars:key1];
    NSMutableArray *hexArray2 = [self splitStringIntoChars:key2];
    
    NSMutableString *str = [NSMutableString new];
    for (int i=0; i<[hexArray1 count]; i++ )
    {
        /*Convert to base 16*/
        int a=(unsigned char)strtol([[hexArray1 objectAtIndex:i] UTF8String], NULL, 16);
        int b=(unsigned char)strtol([[hexArray2 objectAtIndex:i] UTF8String], NULL, 16);
        
        char encrypted = a ^ b;
        [str appendFormat:@"%x",encrypted];
    }
    return str;
}
// decrypting ciphertext and saving apikey text to userdefaults.
-(void)decryptKey {
    NSString *cipher = [[NSUserDefaults standardUserDefaults] objectForKey:CipherText];
    NSString *encryptionKey = [[NSUserDefaults standardUserDefaults] objectForKey:EncryptionKey];
    
    NSString *_apiKey =  [self encryptAndDecrypt:cipher key2:encryptionKey];
    [[NSUserDefaults standardUserDefaults] setObject:_apiKey forKey:ApiKeyText];
}
// Mark : -
// api call for searching movie title.
-(void)getSearchMovie:(NSString*)text {
    [[NSURLSession sharedSession] invalidateAndCancel];
    NSMutableArray *ratings = [[NSMutableArray alloc] init];
    NSMutableDictionary *movieObjects = [[NSMutableDictionary alloc]init];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    __block int apiResponseCount = 0;
    [self decryptKey];
    NSString *_apiKey = [[NSUserDefaults standardUserDefaults] objectForKey:ApiKeyText];
    NSString* urlStr_2017Movie = [NSString stringWithFormat:@"%@search/movie?api_key=%@&primary_release_year=2017&query=%@",baseUrl,_apiKey,text];
    NSString* urlStr_2018Movie = [NSString stringWithFormat:@"%@search/movie?api_key=%@&primary_release_year=2017&query=%@",baseUrl,_apiKey,text];
   
    NSURLSessionDataTask *task_2017Movie = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:urlStr_2017Movie] completionHandler:^(NSData *data, NSURLResponse *response, NSError *err) {
        @synchronized (movieObjects) {
            apiResponseCount += 1;
            [self apiParse:data ratings:ratings movieObject:movieObjects];
            if (apiResponseCount > 1) {
                [self sorting:movieObjects result:result ratings:ratings];

                dispatch_async(dispatch_get_main_queue(), ^{
                    // Update the UI on the main thread.
                [self.delegate getMovieApiCallback:result];
                });

            }
        }
    }];
    NSURLSessionDataTask *task_2018Movie = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:urlStr_2018Movie] completionHandler:^(NSData *data, NSURLResponse *response, NSError *err) {
        @synchronized (movieObjects) {
            apiResponseCount += 1;
            [self apiParse:data ratings:ratings movieObject:movieObjects];
            if (apiResponseCount > 1) {
                [self sorting:movieObjects result:result ratings:ratings];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.delegate getMovieApiCallback:result];
                });
            }
        }
    }];
    [task_2017Movie resume];
    [task_2018Movie resume];
}

// Mark : -
// api call for discovering movie.
-(void)apiCallForDiscoverMovie {
    [[NSURLSession sharedSession] invalidateAndCancel];
    NSMutableDictionary *movieObjects = [[NSMutableDictionary alloc]init];
    NSMutableArray *ratings = [[NSMutableArray alloc]init];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    __block int apiResponseCount = 0;
    [self decryptKey];
    NSString *_apiKey = [[NSUserDefaults standardUserDefaults] objectForKey:ApiKeyText];
    NSString *urlStr_2017Movie = [NSString stringWithFormat:@"%@discover/movie?api_key=%@&primary_release_year=2017&sort_by=popularity.desc",baseUrl,_apiKey];
    NSURLSessionDataTask *task_2017Movie = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:urlStr_2017Movie] completionHandler:^(NSData *data, NSURLResponse *response, NSError *err) {
        @synchronized (movieObjects) {
            apiResponseCount += 1;
        [self apiParse:data ratings:ratings movieObject:movieObjects];
            if (apiResponseCount > 1) {
                [self sorting:movieObjects result:result ratings:ratings];

                dispatch_async(dispatch_get_main_queue(), ^{

                [self.delegate getMovieApiCallback:result];
                    });
            }
        }

    }];
    [task_2017Movie resume];

    NSString *urlStr_2018Movie = [NSString stringWithFormat:@"%@discover/movie?api_key=%@&primary_release_year=2018&sort_by=popularity.desc",baseUrl,_apiKey];
    NSURLSessionDataTask *task_2018Movie = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:urlStr_2018Movie] completionHandler:^(NSData *data, NSURLResponse *response, NSError *err) {
        @synchronized (movieObjects) {
            apiResponseCount += 1;
            [self apiParse:data ratings:ratings movieObject:movieObjects];
            if (apiResponseCount > 1) {
                [self sorting:movieObjects result:result ratings:ratings];
                dispatch_async(dispatch_get_main_queue(), ^{

                [self.delegate getMovieApiCallback:result];
                });
        
            }
        }
        
    }];
    [task_2018Movie resume];
}
-(void)apiParse:(NSData*)data ratings:(NSMutableArray*)ratings movieObject:(NSMutableDictionary*)movieObjects {
    NSDictionary *dict = [self parseResponse:data];
    NSArray *arr = [dict valueForKey:@"results"];
    for (id obj in arr) {
        [ratings addObject:[obj valueForKey:@"vote_average"]];
        [movieObjects setValue:obj forKey:[NSString stringWithFormat:@"%f",[[obj valueForKey:@"vote_average"]floatValue]]];
    }
 
}
-(void)sorting:(NSMutableDictionary*)movieObjects result:(NSMutableArray*)result ratings:(NSMutableArray*)ratings {
    float cArray[ratings.count];
    
    // Fill C-array with ints
    int count = (int)[ratings count];
    
    for (int i = 0; i < count; ++i) {
        cArray[i] = [[ratings objectAtIndex:i] floatValue];
        
    }
    
    mergeSort(cArray, 0, count - 1);
    if (count > totalMovieCountToShow) {
        count = totalMovieCountToShow;
    }
    for (int i = 0; i < count; i += 1) {
        [result addObject:[movieObjects valueForKey:[NSString stringWithFormat:@"%f",cArray[i]]]];
        
    }
}
-(NSDictionary*)parseResponse:(NSData*)data {
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return json;
}
@end
