//
//  DynamicFrameWorkTests.m
//  DynamicFrameWorkTests
//
//  Created by Atul Kumar on 6/14/19.
//  Copyright © 2019 Atul Kumar. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface DynamicFrameWorkTests : XCTestCase {
    NSURLSession *session;
}

@end

@implementation DynamicFrameWorkTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    [super setUp];
    session = [NSURLSession sharedSession];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    session = nil;
    [super tearDown];
}

- (void)testSearchMovieUrl {
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    NSURL *url = [NSURL URLWithString:@"https://api.themoviedb.org/3/search/movie?api_key=c41a5f07878255babc67e2b74d06e1aa&query=king&primary_release_year=2017"];
    XCTestExpectation *expectation = [self expectationWithDescription:@"SearchMovieUrl invoked"];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *err) {
        NSHTTPURLResponse *_resposne = (NSHTTPURLResponse*)response;

        if (err) {
            XCTFail(@"Status code is %ld",_resposne.statusCode);

        }
        else if (_resposne.statusCode == 200) {
            [expectation fulfill];
        }
        else {
            XCTFail(@"Status code is %ld",_resposne.statusCode);
        }
    }];
    [task resume];
    [self waitForExpectations:[NSArray arrayWithObject:expectation] timeout:5.0];
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
